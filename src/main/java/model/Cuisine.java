package model;

import java.util.ArrayList;
import java.util.List;

public class Cuisine {
	private List<Dish> dishes = new ArrayList<Dish>();
	private String name;
	

	public List<Dish> getDishes() {
		return dishes;
	}

	public void setDishes(ArrayList<Dish> dishes) {
		this.dishes = dishes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addDish(Dish dish) {
		dishes.add(dish);
	}

	public void showCuisine() {
		for (Dish dish : dishes) {
			System.out.println("Name: " + dish.getName() + " Price: " + dish.getPrice());
		}
	}

	public Cuisine(List<Dish> dishes, String name) {
		super();
		this.dishes = dishes;
		this.name = name;
	}
	
	

}
