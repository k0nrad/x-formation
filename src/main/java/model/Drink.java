package model;

public class Drink {
	private String name;
	private double price;
	private boolean lemon;
	private boolean ice;
	
	public boolean isLemon() {
		return lemon;
	}
	public void setLemon(boolean lemon) {
		this.lemon = lemon;
	}
	public boolean isIce() {
		return ice;
	}
	public void setIce(boolean ice) {
		this.ice = ice;
	}
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	

}
