package model;

import java.util.ArrayList;
import java.util.List;
public class FillTheData {
	public List<Cuisine> CreateCuisine(){
		List<Dish> polishDishes = new ArrayList<Dish>();
		List<Dish> mexicanDishes = new ArrayList<Dish>();
		List<Dish> italianDishes = new ArrayList<Dish>();
		Dish polChicken = new Dish("Chicken with fries and salad", 12.50);
		Dish polBeef = new Dish("Beef with sauce and potatos", 32.29);
		Dish polTurkey = new Dish("Turkey with vegetables", 22.99);
		Dish polChicken2 = new Dish("Chicken with rice", 9.99);
		Dish polSalad = new Dish("Salad with tuna", 13.30);
		polishDishes.add(polChicken);
		polishDishes.add(polBeef);
		polishDishes.add(polTurkey);
		polishDishes.add(polChicken2);
		polishDishes.add(polSalad);
		
		Dish burrito = new Dish("Burrito", 11.50);
		Dish tacos = new Dish("Tacos", 4.29);
		Dish nachos = new Dish("Nachos", 12.99);
		Dish bean = new Dish("Bean", 2.99);
		Dish tostada = new Dish("Tostada", 13.30);
		mexicanDishes.add(burrito);
		mexicanDishes.add(tacos);
		mexicanDishes.add(nachos);
		mexicanDishes.add(bean);
		mexicanDishes.add(tostada);
		
		Dish pasta = new Dish("Parmesan Garlic Spaghetti", 12.59);
		Dish carbonara = new Dish("Carbonara", 36.39);
		Dish arancini = new Dish("Arancini", 22.99);
		Dish risotto = new Dish("Risotto", 9.99);
		Dish margarita = new Dish("Pizza margarita", 13.30);
		italianDishes.add(pasta);
		italianDishes.add(carbonara);
		italianDishes.add(arancini);
		italianDishes.add(risotto);
		italianDishes.add(margarita);
		
		Cuisine polish = new Cuisine(polishDishes,"Polish");
		Cuisine mexican = new Cuisine(mexicanDishes, "Mexican");
		Cuisine italian = new Cuisine(italianDishes, "Italian");
		
		List<Cuisine> cuisines = new ArrayList<Cuisine>();
		cuisines.add(polish);
		cuisines.add(mexican);
		cuisines.add(italian);
		
		return cuisines;
		
		
	}
	public List<Dish> prepareDeserts(){
		List<Dish> deserts = new ArrayList<Dish>();
		Dish pudding = new Dish("Pudding", 12.59);
		Dish iceCream = new Dish("Ice Cream", 3.39);
		Dish pie = new Dish("Pie", 2.99);
		deserts.add(pudding);
		deserts.add(iceCream);
		deserts.add(pie);
		return deserts;
		
	}
	public List<Dish> prepareDrinks(){
		List<Dish> drinks = new ArrayList<Dish>();
		Dish mohito = new Dish("Mohito", 12.59);
		Dish Wine = new Dish("Wine", 3.39);
		Dish Water = new Dish("Water", 2.99);
		drinks.add(mohito);
		drinks.add(Wine);
		drinks.add(Water);
		return drinks;
		
	}

}
