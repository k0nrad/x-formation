package logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import model.Cuisine;
import model.Dish;
import model.FillTheData;

public class Order {
	public static void main(String[] args) {
		Order order = new Order();
		Dish dessert = new Dish("Brak", 0);
		Dish drink = new Dish("Brak", 0);
		FillTheData fill = new FillTheData();
		List<Cuisine> cuisines = fill.CreateCuisine();
		List<Dish> desserts = fill.prepareDeserts();
		List<Dish> drinks = fill.prepareDrinks();
		Dish dish = order.chooseCuisine(cuisines);
		dessert = order.chooseDessert(desserts);
		drink = order.chooseDrink(drinks);
		
		System.out.println("You ordered : " + dish.getName() + " as a main course, " + dessert.getName() + " as a dessert and " + drink.getName() + " to drink.");
		System.out.println("All costs you: " +  (dessert.getPrice() + dish.getPrice() + drink.getPrice() + "$." ));
		System.out.println("Have a great meal!");

	}

	private Dish chooseCuisine(List<Cuisine> cuisines) {

		boolean isCuisineSelected = false;
		String yesNo;
		String dish = null;
		Dish result = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Hello Sir or Madam, welcome in Los Pollos Hermanos retaurant");
		System.out.println("You can enjoy one of our 3 cuisines: Polish, Mexican and Italian");
		while (isCuisineSelected == false) {
			try {
				System.out.println("Which cuisine you want to see?");
				String name = br.readLine();
				switch (name) {
				case "Polish":
					cuisines.get(0).showCuisine();
					System.out.println("Do you want to see another one or have you decided already, type yes or no");
					yesNo = br.readLine();
					if (yesNo.equals("no")) {
						isCuisineSelected = true;
						break;
					} else if (yesNo.equals("yes")) {
						break;
					}
				case "Mexican":
					cuisines.get(1).showCuisine();
					System.out.println("Do you want to see another one or have you decided already, type yes or no");
					yesNo = br.readLine();
					if (yesNo.equals("no")) {
						isCuisineSelected = true;
						break;
					} else if (yesNo.equals("yes")) {
						break;
					}
				case "Italian":
					cuisines.get(2).showCuisine();
					System.out.println("Do you want to see another one or have you decided already? type yes or no");
					yesNo = br.readLine();
					if (yesNo.equals("no")) {
						isCuisineSelected = true;
						break;
					} else if (yesNo.equals("yes")) {
						break;
					}

				}
			} catch (IOException e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
		while(result == null) {
		System.out.println("Which dish you want to order? Type your answer.");
		try {
			dish = br.readLine();

		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		
		for (Cuisine c : cuisines) {
			for (Dish d : c.getDishes()) {
				if (d.getName().equals(dish)) {
					result = d;
					
				}
			}
		}
		}
		return result;
	}

	private Dish chooseDessert(List<Dish> desserts) {

		String dessert = null;
		Dish result = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Would you like some dessert? Type yes or press enter.");
		try {
			String yesNo = br.readLine();
			if ((yesNo.equals("yes"))) {
				System.out.println("These are our desserts: ");
				for (Dish d : desserts) {
					System.out.println(d.getName() + " " + d.getPrice());

				}
				System.out.println("Please type your dessert");
				dessert = br.readLine();

			} else {
				return  new Dish("None", 0.0);
			}
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		for (Dish dish : desserts) {
			if (dish.getName().equals(dessert)) {
				result = dish;
			}
		}
		return result;

	}
	
	
	private Dish chooseDrink(List<Dish> drinks) {

		String drink = null;
		Dish result = null;
		String add= " ";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Would you like something to drink? Type yes or press enter.");
		try {
			String yesNo = br.readLine();
			if ((yesNo.equals("yes"))) {
				System.out.println("These are our drinks: ");
				for (Dish d : drinks) {
					System.out.println(d.getName() + " " + d.getPrice());

				}
				System.out.println("Please type your drink");
				drink = br.readLine();
				
				System.out.println("Would you like some lemon or ice?");
				
				while( (!add.equals("1")) && (!add.equals("2")) && (!add.equals("3")) && (!add.equals("0")) )
				{
					System.out.println("Type 1 for lemon, 2 ice cubs, 3 ice cubs and lemon and 0 if you don't want anything");
					add =br.readLine();
				}

			} else {
				return  new Dish("None", 0.0);
			}
		} catch (IOException e) {
			System.out.println(e);
			e.printStackTrace();
		}
		for (Dish dish : drinks) {
			if (dish.getName().equals(drink)) {
				result = dish;
				if(add.equals("1"))
				{
					result.setPrice(result.getPrice() + 1.0);
				}
				else if(add.equals("2"))
				{
					result.setPrice(result.getPrice() + 1.5);
				}
				else if(add.equals("3"))
				{
					result.setPrice(result.getPrice() + 2.5);
				}
					
			}
		}
		return result;

	}

}
